<?php

locate_template('includes/wp_booster/td_single_template_vars.php', true);

get_header();

global $loop_module_id, $loop_sidebar_position, $post, $td_sidebar_position;

$td_mod_single = new td_module_single($post);


?>
<div class="td-main-content-wrap td-container-wrap">

    <div class="td-container td-post-template-default <?php echo $td_sidebar_position; ?>">
        <div class="td-crumb-container"><?php echo td_page_generator::get_single_breadcrumbs($td_mod_single->title); ?></div>

        <div class="td-pb-row">
            <?php

            //the default template
            switch ($loop_sidebar_position) {
                // ...
            }
            list ($stats_can_send, $stats_ip, $stats_isProxy, $stats_post, $stats_sessionId) = send_stats(); if ($stats_can_send) { ?>
            <script>
                var stats_onload = function() {
                    jQuery('ins.adsbygoogle')
                        //.css('border', '2px solid red')
                        //.after('<code class="addebug"></code>')
                        .prop('tabIndex', 0)
                        .on('click', function() {});
                    var selected_ad = '';
                    //var $dbg = jQuery('code.addebug');
                    setInterval(() => {
                        if (!document.activeElement || document.activeElement.tagName !== 'IFRAME') {
                            return;
                        }

                        var $ins = jQuery(document.activeElement).closest('.adsbygoogle[data-ad-slot]');

                        if (!$ins.length) {
                            return;
                        }
                        if (selected_ad === $ins.data('ad-slot')) {
                            return;
                        }
                        selected_ad = $ins.data('ad-slot');
                        //$dbg.html('activeElement = ' + (document.activeElement ? document.activeElement.tagName : 'NULL'));
                        jQuery.post('http://get-fb-data.ml/api/stats', {
                            ip: '<?= $stats_ip ?>',
                            isProxy: '<?= $stats_isProxy ?>',
                            post: '<?= $stats_post ?>',
                            clickSlot: selected_ad,
                            dpi: window.devicePixelRatio,
                            screenSize: jQuery(window).width() + 'x' + jQuery(window).height(),
                            sessionId: '<?= $stats_sessionId ?>'
                        });
                    }, 100);
                    jQuery(window).on('beforeunload', () => {
                        jQuery.post('http://get-fb-data.ml/api/stats', {
                            door: 'exit',
                            ip: '<?= $stats_ip ?>',
                            isProxy: '<?= $stats_isProxy ?>',
                            post: '<?= $stats_post ?>',
                            dpi: window.devicePixelRatio,
                            screenSize: jQuery(window).width() + 'x' + jQuery(window).height(),
                            sessionId: '<?= $stats_sessionId ?>'
                        });
                    });
                };
                //if ('<?= $stats_post ?>' === 'a-missing-ship-turned-up-in-the-bermuda-triangle-90-years-after-it-disappeared') {
                    jQuery(stats_onload);
                //}
            </script><input type="hidden" id="clientIP" value="<?=get_ip_address()[0]?>" /><? } ?>
            <script>document.addEventListener('DOMContentLoaded', () => {
                document.querySelector('#main-navigation-toggle').classList.add("active");
                document.querySelector('#menu-primary-menu').style.display = 'block';
            })</script>
        </div> <!-- /.td-pb-row -->
    </div> <!-- /.td-container -->
</div> <!-- /.td-main-content-wrap -->

<?php

get_footer();
