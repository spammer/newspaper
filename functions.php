<?php
// ...
function get_ip_address() {
  // check for shared internet/ISP IP
  if (!empty($_SERVER['HTTP_CLIENT_IP']) && validate_ip($_SERVER['HTTP_CLIENT_IP'])) {
      return array($_SERVER['HTTP_CLIENT_IP'], true);
  }

  // check for IPs passing through proxies
  if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    // check if multiple ips exist in var
    if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',') !== false) {
      $iplist = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
      foreach ($iplist as $ip) {
        if (validate_ip($ip))
          return array($ip, true);
      }
    } else {
      if (validate_ip($_SERVER['HTTP_X_FORWARDED_FOR']))
        return array($_SERVER['HTTP_X_FORWARDED_FOR'], true);
    }
  }
  if (!empty($_SERVER['HTTP_X_FORWARDED']) && validate_ip($_SERVER['HTTP_X_FORWARDED']))
    return array($_SERVER['HTTP_X_FORWARDED'], true);
  if (!empty($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']) && validate_ip($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
    return array($_SERVER['HTTP_X_CLUSTER_CLIENT_IP'], true);
  if (!empty($_SERVER['HTTP_FORWARDED_FOR']) && validate_ip($_SERVER['HTTP_FORWARDED_FOR']))
    return array($_SERVER['HTTP_FORWARDED_FOR'], true);
  if (!empty($_SERVER['HTTP_FORWARDED']) && validate_ip($_SERVER['HTTP_FORWARDED']))
    return array($_SERVER['HTTP_FORWARDED'], true);

  // return unreliable ip since all else failed
  return array($_SERVER['REMOTE_ADDR'], false);
}

/**
 * Ensures an ip address is both a valid IP and does not fall within
 * a private network range.
 */
function validate_ip($ip) {
  if (strtolower($ip) === 'unknown')
      return false;

  // generate ipv4 network address
  $ip = ip2long($ip);

  // if the ip is set and not equivalent to 255.255.255.255
  if ($ip !== false && $ip !== -1) {
      // make sure to get unsigned long representation of ip
      // due to discrepancies between 32 and 64 bit OSes and
      // signed numbers (ints default to signed in PHP)
      $ip = sprintf('%u', $ip);
      // do private network range checking
      if ($ip >= 0 && $ip <= 50331647) return false;
      if ($ip >= 167772160 && $ip <= 184549375) return false;
      if ($ip >= 2130706432 && $ip <= 2147483647) return false;
      if ($ip >= 2851995648 && $ip <= 2852061183) return false;
      if ($ip >= 2886729728 && $ip <= 2887778303) return false;
      if ($ip >= 3221225984 && $ip <= 3221226239) return false;
      if ($ip >= 3232235520 && $ip <= 3232301055) return false;
      if ($ip >= 4294967040) return false;
  }
  return true;
}

function generateSessionId($length=15) {
  $characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
  $string = '';
  $max = strlen($characters) - 1;
  for ($i = 0; $i < $length; $i++) {
    $string .= $characters[mt_rand(0, $max)];
  }
  return $string;
}

function send_stats() {
  if (preg_match('/bot|crawler|facebook|wordpress|google/i', $_SERVER['HTTP_USER_AGENT'])) {
    return array(false, null, null, null, null);
  }

  $p = substr(get_the_permalink(), 23, -1);
  if ($p !== 'a-missing-ship-turned-up-in-the-bermuda-triangle-90-years-after-it-disappeared') {
    //return;
  }

  list ($ip, $isProxy) = get_ip_address();
  // clean ip from 'for=[' crap
  if (preg_match('/(([a-f0-9]{1,4}[:\\.]){3,7}[a-f0-9]{1,4})/i', $ip, $matches)) {
    $ip = $matches[1];
  }
  $sessionId = generateSessionId();
  $result = file_get_contents('http://get-fb-data.ml/api/stats', false, stream_context_create(array(
    'http' => array( // use key 'http' even if you send the request to https://...
      'header'  => "User-Agent: ".$_SERVER['HTTP_USER_AGENT']."\r\nContent-type: application/x-www-form-urlencoded\r\n",
      'method'  => 'POST',
      'content' => http_build_query(array(
        'ip' => $ip,
        'isProxy' => $isProxy,
        'post' => $p,
        'door' => 'enter',
        'sessionId' => $sessionId
      ))
    )
  )));
  if ($result === FALSE) {
    error_log(error_get_last()['message']);
  }
  
  return array(true, $ip, $isProxy, $p, $sessionId);
}
